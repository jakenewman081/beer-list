//Jacob Newman
//2/14/22
//The program is designed for the user to search through a beer list



/** Search through a list of beers to find the one typed in

 * 
 */

/**
 * @author newmanj87939
 *
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class BeerList {

	/**
	 * @param args
	 * @throws FileNotFoundException 
	 * 
	 */
	public static void main(String[] args) throws FileNotFoundException {
		// TODO Auto-generated method stub
		ArrayList<String>beerList= new ArrayList<String>();
		int beerChoice=0; //variable for the choice of beer
		char answer='n'; // variable to determine if this is the choice the user wanted
		File file =new File("beers.txt"); // inputs the file
		Scanner sc= new Scanner(file); // reads the file
		Scanner read=new Scanner(System.in);
		
		while (sc.hasNextLine()) // creates beer list from txt document 
		beerList.add(sc.nextLine());
		
		System.out.println("These are all the beers we have in stock. Please look at the list and decide."+"\n");
		
		
	do  { //loop for printing beer and choosing beer
	for(int b=0;b<beerList.size();b++) { //displays list of beer
		
		String current=beerList.get(b);
		System.out.println(current.substring(0,current.indexOf("/"))+"\n");
	}
	
	System.out.print("Please enter the number that is next to the beer to see more information."+"\n"); //choosing beer
	beerChoice= read.nextInt();
	System.out.println(beerList.get(beerChoice-1));
	System.out.print("Is this the beer you would like,please enter Y or N."); // Y or N for beer
	answer=read.next().charAt(0);
	
	if (answer=='Y' || answer=='y') {
		System.out.println("Good choice, our waiter will be out shortly");
	
	}
	
	

	}while (answer=='n' || answer=='N');

	}
	
	
}



